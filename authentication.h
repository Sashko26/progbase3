#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H

#include <QDialog>
#include <storage_lib/optional.h>
#include <storage_lib/user.h>
#include <QString>
#include "storage_lib/storage.h"


namespace Ui {
class authentication;
}

class authentication : public QDialog
{
    Q_OBJECT

public:
    explicit authentication(Storage * storage,QWidget *parent = 0);
    ~authentication();
    QString getPassword();
    QString getLogin();
    void textBrowserInCorrect();


private slots:


    void on_Button_registration_clicked();

private:
    Ui::authentication *ui;
     Storage * storage_;
};

#endif // AUTHENTICATION_H
