#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>



#include <storage_lib/metroStation.h>




namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = 0);
    void setData(metroStation metrostation);
    bool checkLinesInAddDialog();
    ~AddDialog();
    metroStation data();

private:
    Ui::AddDialog *ui;
};

#endif // ADDDIALOG_H
