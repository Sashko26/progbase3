#include "editdialog.h"
#include "ui_editdialog.h"
#include "storage_lib/storage.h"

EditDialog::EditDialog(Storage * storage ,int idEditSt, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);
    this->storPtr = storage;
    this->idEditSt =idEditSt;
}

EditDialog::~EditDialog()
{
    delete ui;
}
void EditDialog::fillingLabebsInEditDialogDefaultData(metroStation st)
{
    QString nameOfStation =QString::fromStdString(st.nameOfStation);
    QString nameOfLine=QString::fromStdString(st.nameOfLine);
    int openingTime=st.hourOfOpening;
    this->ui->lineEdit_stName->setText(nameOfStation);


    //this->ui->lineEdit_openTime->setText(openingTime);
    this->ui->spinBox->setValue(openingTime);


    this->ui->lineEdit_lineName->setText(nameOfLine);
}
bool EditDialog::checkLinesInEditDialog()
{
    QString stationName=this->ui->lineEdit_stName->text();


    //QString openingTime=this->ui->lineEdit_openTime->text();


    QString lineName=this->ui->lineEdit_lineName->text();
    if(stationName.size()==0 || lineName.size()==0)
    {
        return false;
    }
    return true;
}
metroStation EditDialog::getInstance()
{
    metroStation station;
    string st_name = this->ui->lineEdit_stName->text().toStdString();
    string line_name = this->ui->lineEdit_lineName->text().toStdString();


    //int open_time=this->ui->lineEdit_openTime->text().toInt();
    int open_time=this->ui->spinBox->value();

    station.nameOfStation=st_name;
    station.hourOfOpening=open_time;
    station.nameOfLine=line_name;
    return station;
}

//встановлення даних однієї сутності у лістВіджет_лайнсОфСтейшн
void EditDialog::setDataLine(metroLine metroline)
{
    QVariant qVariant;
        qVariant.setValue(metroline);

        QListWidgetItem *qMetroLineListItem = new QListWidgetItem();
        QString q_name_of_line=QString::fromStdString(metroline.nameOfline);
        qMetroLineListItem ->setText(q_name_of_line);
        qMetroLineListItem ->setData(Qt::UserRole, qVariant);
        ui->listWidget_lineOfstation->addItem(qMetroLineListItem);
 }


void EditDialog::setDataLineHaveNot(metroLine metroline)
{
        QVariant qVariant;
        qVariant.setValue(metroline);

        QListWidgetItem *qMetroLineListItem = new QListWidgetItem();
        QString q_name_of_line=QString::fromStdString(metroline.nameOfline);
        qMetroLineListItem ->setText(q_name_of_line);
        qMetroLineListItem ->setData(Qt::UserRole, qVariant);
        ui->listWidget_stationHaveNotLines->addItem(qMetroLineListItem);
 }


//заповнення ліст віджет лініями метро , які належать станції.
void EditDialog::fillWidgetList(vector<metroLine> metrolines,bool have)
{
        if(have==true)
        {
            if(this->ui->listWidget_lineOfstation->count()!=0)
            {
                ui->listWidget_lineOfstation->clear();

            }
            for(int i=0;i<static_cast<int>(metrolines.size());i++)
            {
                metroLine metroline=metrolines[i];
                this->setDataLine(metroline);
            }
        }
         else if(have==false)
        {
            if(this->ui->listWidget_lineOfstation->count()!=0)
            {
                ui->listWidget_stationHaveNotLines->clear();

            }
            for(int i=0;i<static_cast<int>(metrolines.size());i++)
            {
                metroLine metroline=metrolines[i];
                this->setDataLineHaveNot(metroline);
            }
        }

}


void EditDialog::on_addLinks_button_clicked()
{
    QList<QListWidgetItem *> items =ui->listWidget_stationHaveNotLines->selectedItems();
    if(items.count() == 0)
    {
        qDebug()<< "nothing selected";
    }
    else
    {
            foreach (QListWidgetItem * selectedItem, items)
        {
            int row_index =ui->listWidget_stationHaveNotLines->row(selectedItem);
            ui->listWidget_stationHaveNotLines->takeItem(row_index);
            QVariant var = selectedItem->data(Qt::UserRole);
            metroLine line = var.value<metroLine>();
            int id =line.id;
            this->storPtr->insertLineinStation(this->idEditSt,id);
            //вставляння лінії метро в лістВіджет (зв'язок станція-лінія)
            this->setDataLine(line);

            delete selectedItem;
        }

    }
}

void EditDialog::on_deleteLinks_button_clicked()
{
    QList<QListWidgetItem *> items =ui->listWidget_lineOfstation->selectedItems();
    if(items.count() == 0)
    {
        qDebug()<< "nothing selected";
    }



    foreach (QListWidgetItem * selectedItem, items)
{
    int row_index =ui->listWidget_lineOfstation->row(selectedItem);
    ui->listWidget_lineOfstation->takeItem(row_index);
    QVariant var = selectedItem->data(Qt::UserRole);
    metroLine line = var.value<metroLine>();
    int id =line.id;
    this->storPtr->removeLineinStation(this->idEditSt,id);
    //вставляння лінії метро в лістВіджет (зв'язок станція-лінія)
    this->setDataLineHaveNot(line);
    delete selectedItem;
}
}


