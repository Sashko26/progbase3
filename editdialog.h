#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <storage_lib/metroStation.h>
#include <storage_lib/metroLine.h>
#include "storage_lib/storage.h"

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(Storage * storage,int idEditSt, QWidget *parent = 0);
    void fillingLabebsInEditDialogDefaultData(metroStation st);
    bool checkLinesInEditDialog();
    metroStation getInstance();
    void fillWidgetList(vector<metroLine> metrolines, bool have);
    void setDataLine(metroLine metroline);
    void setDataLineHaveNot(metroLine metroline);
    ~EditDialog();

private slots:
   // void on_buttonBox_clicked(QAbstractButton *button);



    void on_addLinks_button_clicked();

    void on_deleteLinks_button_clicked();

private:
    Ui::EditDialog *ui;
    Storage *storPtr;
    int idEditSt;
    int stationId;
};

#endif // EDITDIALOG_H
