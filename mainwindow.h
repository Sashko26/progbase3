#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <storage_lib/storage.h>
#include <QListWidget>
#include <QtXml>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setData(metroStation metrostation);
    void labelCurrentPageMetroStation();
    void fillPageListWidgetWithMetroStation();
    void textInLabelCurrentPage();
    bool openXMLfileForExport();
    QDomElement metroStationToDomElement(QDomDocument & doc, metroStation & c);
    metroStation domElementToMetroStation(QDomElement & element);
    bool openXMLfileForImport();



    void clear();
    ~MainWindow();

private slots:
    void New_Storage();
    void Open_Storage();
    void beforeClose();


    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_addButton_clicked();

    void on_removeButton_clicked();

    void on_editButton_clicked();

    void on_Button_nextPage_clicked();

    void on_button_prevPage_clicked();


    void on_lineEdit_searchForNameOfstation_textChanged(const QString &arg1);




    bool on_button_photo_clicked();

private:
    Ui::MainWindow *ui;
    Storage * storage_; // <-- STORAGE
    int pages_size=5;
    int items_total=0;
    int current_page=0;

    int current_pageSearchMode=0;
    int items_totalSearchMode=0;
    bool searchMode=false;

    //int user_id;
};

#endif // MAINWINDOW_H
