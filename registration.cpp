#include "registration.h"
#include "ui_registration.h"

registration::registration(Storage * _storage,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::registration)
{
    ui->setupUi(this);
    this->_storage=_storage;
    ui->textBrowser_inCorrect_regist->setVisible(false);
}

registration::~registration()
{
    delete ui;
}
void registration::textBrowserInCorrect()
{
    ui->textBrowser_inCorrect_regist->setVisible(true);
    ui->textBrowser_inCorrect_regist->setText("Логін або пароль не є унікальним,\n"
                                              " або паролі не співпадають");
    ui->lineEdit_login->clear();
    ui->lineEdit_password->clear();
    ui->lineEdit_repeadPassword->clear();
}
QString registration::getLogin()
{
   return ui->lineEdit_login->text();
}
QString registration::getPassword()
{
   return ui->lineEdit_password->text();
}
QString registration::getRepeadPassword()
{
    return ui->lineEdit_repeadPassword->text();
}




