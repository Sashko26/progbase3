#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QString>
#include <vector>
#include <math.h>


#include <QPixmap>



#include "sqlite_storage.h"


#include <storage_lib/storage.h>


#include "adddialog.h"
#include "editdialog.h"
#include "authentication.h"
#include <QFile>


void MainWindow::clear()
{
    ui->editButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
    ui->label_StationName->clear();
    ui->label_LineName->clear();
    ui->label_amountOfLinks->clear();
    ui->label_openTime->clear();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
     ui->setupUi(this);
     authentication * auth= new authentication(this->storage_,this);

     bool inCorrectData=true;
     bool label_for_incorrectData=false;
     while(inCorrectData)
     {
            int status;
                    if(label_for_incorrectData)
                    {
                        auth->textBrowserInCorrect();
                        status=auth->exec();

                    }
                    else
                    {
                        status=auth->exec();
                    }

            if(status==1)
            {
                QString login = auth->getLogin();
                QString password =auth->getPassword();

                    qDebug()<<login;
                    qDebug()<<password;
                    SqliteStorage * sqlstorage= new SqliteStorage("../progbase3/data/sql");
                    Storage * storage= sqlstorage;
                    this->storage_=storage;
                    this->storage_->open();
                    qDebug() << (this->storage_ == nullptr);
                    optional<User> user=this->storage_->getUserAuth(login,password);
                    if(user!=nullopt)
                        qDebug()<<user.value().username;
                 if(user!=nullopt)
                 {
                     inCorrectData=false;
                     connect(ui->actionNew_Storage, &QAction::triggered, this, &MainWindow::New_Storage);
                     connect(ui->actionExit, &QAction::triggered, this, &MainWindow::beforeClose);
                     connect(ui->actionOpen_Storage, &QAction::triggered, this, &MainWindow::Open_Storage);
                     connect(ui->actionExport_table, &QAction::triggered, this, &MainWindow::openXMLfileForExport);
                     connect(ui->actionImport_table_from_XML,&QAction::triggered,this,&MainWindow::openXMLfileForImport);
                     ui->addButton->setEnabled(false);
                     ui->editButton->setEnabled(false);
                     ui->removeButton->setEnabled(false);
                     ui->button_photo->setEnabled(false);
                     storage->open();



                    User currentUser =user.value();
                    qDebug()<< currentUser.id;
                    int userId=currentUser.id;
                    storage_->user_id=userId;
                    qDebug()<<storage_->user_id;

                    current_page+=1;
                    labelCurrentPageMetroStation();

                     this->items_total=storage_->getAmountUserStation(storage_->user_id);
                    if(items_total<=pages_size)
                    {
                        ui->Button_nextPage->setEnabled(false);
                    }
                    else
                    {
                        ui->Button_nextPage->setEnabled(true);
                    }
                    ui->button_prevPage->setEnabled(false);

                    if(this->ui->listWidget->count()!=0)
                    {
                        ui->listWidget->clear();

                    }
                        vector<metroStation> stations=this->storage_->getAllUserMetroStations(userId);
                        stations=this->storage_->getAllUserMetroStations(userId,current_page,pages_size);
                    for(int i=0;i<static_cast<int>(stations.size());i++)
                       {
                           metroStation metrostation=stations[i];
                           this->setData(metrostation);
                       }
                       ui->addButton->setEnabled(true);
                 }
                 else
                 {
                     label_for_incorrectData=true;
                 }

            }
            else
                {
                    auth->close();
                    inCorrectData=false;
                        exit(0);
                }
     }













}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}


void MainWindow::beforeClose()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this, "On exit", "Are you sure?");

    if(reply==QMessageBox::StandardButton::Yes)
    {
        this->close();
    }
    else
    {
        qDebug() << "Do not exit";
    }

}
//встановлення нової сутності в listWidget/
void MainWindow::setData(metroStation metrostation)
{
    QVariant qVariant;
        qVariant.setValue(metrostation);

        QListWidgetItem *qMetroStationListItem = new QListWidgetItem();
        QString q_name_of_station=QString::fromStdString(metrostation.nameOfStation);
        qMetroStationListItem->setText(q_name_of_station);
        qMetroStationListItem->setData(Qt::UserRole, qVariant);
        ui->listWidget->addItem(qMetroStationListItem);
 }

// кнопка у лівому верхньому куті, що відповідає за відкриття діалогу з папками для Стореджу, в цій лабі не юзається!
void MainWindow::Open_Storage()
{
    if(this->storage_!=nullptr)
    {
        delete this->storage_;
    }

     QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                  "../",
                QFileDialog::ShowDirsOnly
           | QFileDialog::DontResolveSymlinks);
    qDebug() << dir;
    string std_dir=dir.toStdString();
    Storage * storage=new SqliteStorage(std_dir);
    this->storage_=storage;
    storage->open();
    vector<metroStation> stations=this->storage_->getAllmetroStations();
    if(this->ui->listWidget->count()!=0)
    {
        ui->listWidget->clear();
    }
    for(int i=0;i<static_cast<int>(stations.size());i++)
        {
            metroStation metrostation=stations[i];
            this->setData(metrostation);
        }
        ui->addButton->setEnabled(true);

}
void MainWindow::New_Storage()
{   if(this->storage_!=nullptr)
    {
        delete this->storage_;
    }
     QFileDialog dialog(this);
      dialog.setFileMode(QFileDialog::Directory);
      QString current_dir = QDir::currentPath();
      QString default_name = "new_storage";
      QString folder_path = dialog.getSaveFileName(
          this,
          "Select New Storage Folder",
          current_dir + "/" + default_name,
          "Folders"); qDebug() << folder_path;
        QDir().mkdir(folder_path);
        QString fname = folder_path+"/metroStation.xml";
        QFile file(fname);
        if (file.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            file.close();
        }


        string std_folder_path=folder_path.toStdString();
        storage_= new SqliteStorage(std_folder_path);
        storage_->open();
        vector<metroStation> stations=this->storage_->getAllmetroStations();
        if(this->ui->listWidget->count()!=0)
        {
            ui->listWidget->clear();
        }
        for(int i=0;i<static_cast<int>(stations.size());i++)
           {
               metroStation metrostation=stations[i];
               this->setData(metrostation);
           }
           ui->addButton->setEnabled(true);



           fname = folder_path+"/id.txt";
        QFile fileId(fname);
        if (fileId.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            fileId.write("1");
            fileId.close();
        }




}
bool MainWindow::openXMLfileForExport()
{
   QString fileName = QFileDialog::getOpenFileName(
               this,              // parent
               "export in xml",  // caption
               "",                // directory to start with
               "XML (*.xml);;All Files (*)");  // file name filter
   qDebug() << fileName;
   if(fileName.size()==0)
   {
       return false;
   }
   if(!fileName.contains(".xml"))
   {
       return false;
   }
  vector<metroStation> stations= storage_->getAllUserMetroStations(storage_->user_id,0,0,false,"x");
     qDebug()<<stations.size()<<"ROZMIR";
   QDomDocument doc;
   QDomElement root=doc.createElement("metroStations");
   for(metroStation & c: stations)
   {
         QDomElement metroStation_el=metroStationToDomElement(doc,c);
        root.appendChild(metroStation_el);
   }
   doc.appendChild(root);
   QString xml_text =doc.toString(4);
     QFile file(fileName);
     bool is_opened=file.open(QFile::WriteOnly);

 if(!is_opened)
   {
       qDebug()<< "file not opened for writing" <<fileName;
       abort();
   }
 QTextStream ts(&file);
 ts << xml_text;
 file.close();
 return true;
}
bool MainWindow::openXMLfileForImport()
{
    QString fileName = QFileDialog::getOpenFileName(
                this,              // parent
                "import from xml",  // caption
                "",                // directory to start with
                "XML (*.xml);;All Files (*)");  // file name filter
    qDebug() << fileName;

               if(fileName.size()==0)
               {
                   return false;
               }
               if(!fileName.contains(".xml"))
               {
                   return false;
               }
              QFile file(fileName);
              bool is_opened=file.open(QFile::ReadOnly);

          if(!is_opened)
            {
                qDebug()<< "file not opened" <<fileName;
                return false;
            }
            QTextStream ts(&file);
            QString text=ts.readAll();
              file.close();
            QDomDocument doc;
            QString errorMessage;
            int errorLine;
            int errorColumn;
            if(!doc.setContent(text, &errorMessage, &errorLine , &errorColumn))
            {
                  qDebug()<<"Error parsing XML text"<< errorMessage;
                  qDebug()<<"at line"<< errorLine<<"column "<< errorColumn;
                  return false;
            }
            vector <metroStation> stations;
            QDomElement root =doc.documentElement();
            for(int i=0;i< root.childNodes().size();i++)
            {
              QDomNode node = root.childNodes().at(i);
              QDomElement element = node.toElement();
              metroStation c=domElementToMetroStation(element);
              stations.push_back(c);
          }
            file.close();


                for(int i=0;i<stations.size();i++)
                {
                    storage_->insertmetroStation(stations[i],storage_->user_id);
                }
                 int amountInstance_ofcurrentLisrWidget = ui->listWidget->count();
                 qDebug()<<amountInstance_ofcurrentLisrWidget;

                if(amountInstance_ofcurrentLisrWidget<pages_size)
                {
                    for(int i=0;i<pages_size-amountInstance_ofcurrentLisrWidget;i++)
                    {
                        setData(stations[i]);
                    }
                }
                this->items_total=storage_->getAmountUserStation(storage_->user_id);
                labelCurrentPageMetroStation();


                if(current_page==1)
                {
                    ui->button_prevPage->setEnabled(false);
                }
                if(items_total>current_page*pages_size)
                {
                    ui->Button_nextPage->setEnabled(true);
                }

            return true;

}


QDomElement MainWindow::metroStationToDomElement(QDomDocument & doc, metroStation & c)
{
    QDomElement metroStation_el = doc.createElement("metroStation");
    metroStation_el.setAttribute("id", c.id);
    metroStation_el.setAttribute("nameOfStation", c.nameOfStation.c_str());
    metroStation_el.setAttribute("hourOfOpening", c.hourOfOpening);
    metroStation_el.setAttribute("nameOfLine", c.nameOfLine.c_str());
    return metroStation_el;
}

metroStation MainWindow::domElementToMetroStation(QDomElement & element)
{
    metroStation c;
    c.id=element.attribute("id").toInt();
    c.nameOfStation=element.attribute("nameOfStation").toStdString();
    c.hourOfOpening=element.attribute("hourOfOpening").toInt();
    c.nameOfLine=element.attribute("nameOfLine").toStdString();
     return c;
}



void MainWindow::on_listWidget_itemClicked(QListWidgetItem *items)
{

    if(items->isSelected()==false)
    {

        ui->button_photo->setEnabled(false);
        QList<QListWidgetItem *> list_items =ui->listWidget->selectedItems();
        if(list_items.size()==0)
        {
             this->clear();
             ui->label_photo->clear();

        }
        else
        {
            ui->button_photo->setEnabled(true);
            ui->label_StationName->clear();
            ui->label_openTime->clear();
            QVariant var = list_items[list_items.size()-1]->data(Qt::UserRole);
            metroStation st = var.value<metroStation>();
            storage_->metroStation_id=st.id;
            QString q_st_name=QString::fromStdString(st.nameOfStation);
            ui->label_StationName->setText("Station name: " + q_st_name);
            QString  q_st_openingtime=QString::number(st.hourOfOpening);
            ui->label_openTime->setText("Opening time: "+ q_st_openingtime);
            QString q_st_linename=QString::fromStdString(st.nameOfLine);
            ui->label_LineName->setText("Line name: " + q_st_linename);
            storage_->amountLinks(storage_->metroStation_id);
            ui->label_amountOfLinks->setText("amount links: " + QString::number(storage_->amountlinks));
            QByteArray arr =storage_->getPhoto(st.id);
            QPixmap tmp = QPixmap();
            tmp.loadFromData(arr);
            if(tmp.isNull())
            {
                qDebug()<<"hello";
            }
            ui->label_photo->setPixmap(tmp.scaled(ui->label_photo->width(), ui->label_photo->height(), Qt::KeepAspectRatio));
            ui->label_photo->show();
        }

    }

    else
    {
          ui->button_photo->setEnabled(true);
          QVariant var = items->data(Qt::UserRole);
          metroStation st = var.value<metroStation>();
         QByteArray arr =storage_->getPhoto(st.id);
         QPixmap tmp = QPixmap();
         tmp.loadFromData(arr);
         if(tmp.isNull())
         {
             qDebug()<<"hello";
         }
          ui->label_photo->setPixmap(tmp.scaled(ui->label_photo->width(), ui->label_photo->height(), Qt::KeepAspectRatio));
          ui->label_photo->show();
          storage_->metroStation_id=st.id;
          QString q_st_name=QString::fromStdString(st.nameOfStation);
          ui->label_StationName->setText("Station name: " + q_st_name);
          QString q_st_openingtime=QString::number(st.hourOfOpening);
          ui->label_openTime->setText("Opening time: " + q_st_openingtime);
          QString q_st_linename=QString::fromStdString(st.nameOfLine);
          ui->label_LineName->setText("Line name: " + q_st_linename);
          storage_->amountLinks(storage_->metroStation_id);
          ui->label_amountOfLinks->setText("amount links: " + QString::number(storage_->amountlinks));
          ui->editButton->setEnabled(true);
          ui->removeButton->setEnabled(true);
     }

}

void MainWindow::on_addButton_clicked()
{
    AddDialog addDialog(this);
    int status=addDialog.exec();
       if(status==1)
     {
           if(addDialog.checkLinesInAddDialog()==true)
           {
               metroStation station=addDialog.data(); //зчитали дані з полів
               if(this->storage_==nullptr)
               {
                   qDebug()<<"hi man!";
               }
               qDebug()<<"again"<<storage_->user_id;
               qDebug()<<storage_;
               this->storage_->insertmetroStation(station, storage_->user_id); //вставили нову сутність  у вектор у стореджі і добавили її у файл
               this->setData(station); // добавили сутність у ЛістВіджет
           }

     }
qDebug() << status;
}
void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> items =ui->listWidget->selectedItems();
    if(items.count() == 0)
    {
        qDebug()<< "nothing selected";
    }
    else
    {
        foreach (QListWidgetItem * selectedItem, items)
        {
            int row_index =ui->listWidget->row(selectedItem);
            ui->listWidget->takeItem(row_index);
            QVariant var = selectedItem->data(Qt::UserRole);
            metroStation st = var.value<metroStation>();
            int id =st.id;
            this->storage_->removemetroStation(id);
            delete selectedItem;
        }
       vector <metroStation> stationsForFillingCurrentPage=storage_->getAllUserMetroStations(storage_->user_id,current_page,pages_size,false);
       if(ui->listWidget->count()<pages_size && current_page*pages_size<storage_->getAmountUserStation(storage_->user_id))
       {
           for(int i=ui->listWidget->count();i<pages_size;i++)
           {
               setData(stationsForFillingCurrentPage[i]);
           }
       }
       if(storage_->getAmountUserStation(storage_->user_id,false)==current_page*pages_size)
       {
           ui->Button_nextPage->setEnabled(false);
       }


    }

    if(ui->listWidget->selectedItems().count()==0)
    {
        ui->editButton->setEnabled(false);
        ui->removeButton->setEnabled(false);
        ui->label_StationName->clear();
        ui->label_openTime->clear();
        ui->label_LineName->clear();
        ui->label_photo->clear();
        ui->button_photo->setEnabled(false);
        ui->label_amountOfLinks->clear();

    }
}

void MainWindow::on_editButton_clicked()
{



    QListWidgetItem* stationInWidgetList=ui->listWidget->selectedItems()[ui->listWidget->selectedItems().size()-1];
    QVariant var=stationInWidgetList->data(Qt::UserRole);
    metroStation st = var.value<metroStation>();
    int stationId=st.id;
    EditDialog editDialog(this->storage_,stationId, this);
    editDialog.fillingLabebsInEditDialogDefaultData(st);


    vector<metroLine> metroLines = storage_->getAllLinesOfstation(storage_->metroStation_id);
    bool have= true;

   editDialog.fillWidgetList(metroLines,have);

   vector<metroLine> metroLinesHaveNot =storage_->getAllHaveNotLinesOfstation(storage_->metroStation_id);
   have=false;
   editDialog.fillWidgetList(metroLinesHaveNot,have);



    int status=editDialog.exec();



    if(status==1)
    {
        if(editDialog.checkLinesInEditDialog()==true)
        {
            metroStation station;
            station=editDialog.getInstance();
            this->storage_->updatemetroStation(station);

            qDebug()<<QString::fromStdString(station.nameOfStation);
            qDebug()<<QString::fromStdString(station.nameOfLine);
            qDebug()<<station.hourOfOpening;
            this->ui->label_StationName->setText("Station name: "+QString::fromStdString(station.nameOfStation));
            this->ui->label_LineName->setText("Line time: "+QString::fromStdString(station.nameOfLine));
            this->ui->label_openTime->setText("Opening time: "+QString::number(station.hourOfOpening));
            ui->listWidget->selectedItems()[ui->listWidget->selectedItems().size()-1]->setText(QString::fromStdString(station.nameOfStation));
            QVariant qVariant;
            qVariant.setValue(station);
            ui->listWidget->selectedItems()[ui->listWidget->selectedItems().size()-1]->setData(Qt::UserRole, qVariant);
        }
    }
    else
    {
        qDebug()<<"changes opinion";
    }
}

void MainWindow::on_Button_nextPage_clicked()
{
    if(searchMode)
    {
        current_pageSearchMode+=1;
        fillPageListWidgetWithMetroStation();
        int amountAllUserStations=storage_->getAmountUserStation(storage_->user_id,true,ui->lineEdit_searchForNameOfstation->text());
        double total_pages=ceil(static_cast<float>(amountAllUserStations)/static_cast<float>(pages_size));

        QString arg1=ui->lineEdit_searchForNameOfstation->text();
        vector<metroStation> resultOfSearching = storage_->searchForSubStr(arg1,pages_size,current_pageSearchMode);
        ui->listWidget->clear();
        for(int i=0;i<resultOfSearching.size();i++)
        {
            setData(resultOfSearching[i]);
        }
       textInLabelCurrentPage();



        if(current_pageSearchMode==total_pages)
        {
            ui->Button_nextPage->setEnabled(false);
        }
        if(current_pageSearchMode!=1)
        {
            ui->button_prevPage->setEnabled(true);
        }
    }
    else
    {
            current_page+=1;
            fillPageListWidgetWithMetroStation();
            int amountAllUserStations=storage_->getAmountUserStation(storage_->user_id,false);
            double total_pages=ceil(static_cast<float>(amountAllUserStations)/static_cast<float>(pages_size));
            if(current_page==total_pages)
            {
                ui->Button_nextPage->setEnabled(false);
            }
            if(current_page!=1)
            {
                ui->button_prevPage->setEnabled(true);
            }
    }

        labelCurrentPageMetroStation();

}
void MainWindow::labelCurrentPageMetroStation()
{
    ui->label_currentPage->clear();
    textInLabelCurrentPage();
}

void MainWindow::on_button_prevPage_clicked()
{
    if(searchMode)
    {
        current_pageSearchMode-=1;
       fillPageListWidgetWithMetroStation();
       int amountAllUserStations=storage_->getAmountUserStation(storage_->user_id,true);
        double total_pages=ceil(static_cast<float>(amountAllUserStations)/static_cast<float>(pages_size));
           if(current_pageSearchMode==1)
           {
               ui->button_prevPage->setEnabled(false);
           }
           if(current_pageSearchMode!=total_pages)
           {
               ui->Button_nextPage->setEnabled(true);
           }
    }
    else
    {
        current_page-=1;
       fillPageListWidgetWithMetroStation();
       int amountAllUserStations=storage_->getAmountUserStation(storage_->user_id,false);
        double total_pages=ceil(static_cast<float>(amountAllUserStations)/static_cast<float>(pages_size));
           if(current_page==1)
           {
               ui->button_prevPage->setEnabled(false);
           }
           if(current_page!=total_pages)
           {
               ui->Button_nextPage->setEnabled(true);
           }
    }
    labelCurrentPageMetroStation();
}


void MainWindow::on_lineEdit_searchForNameOfstation_textChanged(const QString &arg1)
{

    if(ui->lineEdit_searchForNameOfstation->text().size()!=0)
    {
        searchMode=true;
        current_pageSearchMode=1;
        qDebug()<<current_pageSearchMode;
        qDebug()<<pages_size;
        ui->listWidget->clear();
        vector<metroStation> resultOfSearching = storage_->searchForSubStr(arg1,pages_size,current_pageSearchMode);
        for(int i=0;i<resultOfSearching.size();i++)
        {
            setData(resultOfSearching[i]);
        }
       textInLabelCurrentPage();

       if(current_pageSearchMode==1)
       {
          ui->button_prevPage->setEnabled(false);
       }

    }
    else if(ui->lineEdit_searchForNameOfstation->text().size()==0)
    {
        searchMode=false;
        fillPageListWidgetWithMetroStation();
        textInLabelCurrentPage();
        current_pageSearchMode=0;
        items_total=storage_->getAmountUserStation(storage_->user_id);
        if(current_page*pages_size>=items_total)
        {
            ui->Button_nextPage->setEnabled(false);
            qDebug()<<items_total<<" -amount of all items";
            qDebug()<<current_page<<" -current page";
            qDebug()<<pages_size<<" -page size";
        }
        else
        {
            ui->Button_nextPage->setEnabled(true);
        }
        if(current_page==1)
        {
            ui->button_prevPage->setEnabled(false);
        }
        else
        {
            ui->button_prevPage->setEnabled(true);
        }
    }



}
void MainWindow::fillPageListWidgetWithMetroStation()
{
    ui->listWidget->clear();
    vector<metroStation> stations;
    if(searchMode)
    {
        stations =storage_->getAllUserMetroStations(storage_->user_id,current_page,pages_size,true,ui->lineEdit_searchForNameOfstation->text());
    }
    else
    {
         stations =storage_->getAllUserMetroStations(storage_->user_id,current_page,pages_size,false);
    }
       for(int i=0;i<static_cast<int>(stations.size());i++)
       {
           metroStation metrostation=stations[i];
           this->setData(metrostation);
       }
}
void MainWindow::textInLabelCurrentPage()
{   if(searchMode)
    {
        QString current_pageStr=QString::number(current_pageSearchMode);

        QString subStr =ui->lineEdit_searchForNameOfstation->text();
        QString totalPagesStr=QString::number(ceil(   static_cast<float>(storage_->getAmountUserStation(storage_->user_id,true,subStr))   /   static_cast<float>(pages_size)    ) );
        items_totalSearchMode=storage_->getAmountUserStation(storage_->user_id,true,subStr);
        qDebug()<<static_cast<float>(pages_size);
        qDebug()<<"hello "<<static_cast<float>(storage_->getAmountUserStation(storage_->user_id,true,subStr));
        qDebug()<<totalPagesStr;
        ui->label_currentPage->setText("searchMode:"+current_pageStr+"/"+totalPagesStr);
    }
    else
    {
        QString current_pageStr=QString::number(current_page);
        QString totalPagesStr=QString::number(ceil(   static_cast<float>(storage_->getAmountUserStation(storage_->user_id,false))   /   static_cast<float>(pages_size)    ) );

        qDebug()<<storage_->getAmountUserStation(storage_->user_id,false)<<" - Amount of all user station ";
        qDebug()<<pages_size<<"- size of page";

        ui->label_currentPage->setText("             "+current_pageStr+"/"+totalPagesStr);
    }
}



bool MainWindow::on_button_photo_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(
                   this,              // parent
                   "get Picture",  // caption
                   "",                // directory to start with
                   "XML (*.xml);;All Files (*)");  // file name filter

       qDebug() << fileName;
       if(fileName.size()==0)
       {
           return false;
       }

       if(!fileName.contains(".jpg") && !fileName.contains(".png"))
       {
           return false;
       }
       QFile file(fileName);

       file.open(QIODevice::ReadOnly);

       QByteArray arr = file.readAll();//;= fileName; // записать фото
       QPixmap tmp = QPixmap();
       tmp.loadFromData(arr);
       ui->label_photo->setPixmap(tmp.scaled(ui->label_photo->width(), ui->label_photo->height(), Qt::KeepAspectRatio));
       ui->label_photo->show();
       QList<QListWidgetItem *> list_items =ui->listWidget->selectedItems();
       QVariant var = list_items[list_items.size()-1]->data(Qt::UserRole);
       metroStation st = var.value<metroStation>();
       if(arr.isEmpty())
       {
            qDebug()<<"binary file of picture is empty!";
       }

       storage_->insertPhoto(st.id,fileName);
       return true;
}
