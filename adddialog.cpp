#include "adddialog.h"
#include "ui_adddialog.h"
#include <mainwindow.h>
#
AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}
 //cтворює нову сутність, зчитавши дані з полів для вводу тексту.
metroStation AddDialog::data()
{
    metroStation station;

    string std_nameStation=ui->lineEdit_addnameST->text().toStdString();


    int openingTime=ui->spinBox->value();              //text().toInt();



    string std_nameLine=ui->lineEdit_addNameLine->text().toStdString();
    station.nameOfStation=std_nameStation;
    station.hourOfOpening=openingTime;
    station.nameOfLine=std_nameLine;
    return station;
}
bool AddDialog::checkLinesInAddDialog()
{
    QString stationName=this->ui->lineEdit_addnameST->text();


    //QString openingTime=this->ui->lineEdit_addOpeningTime->text();

   // int openingTime=this->ui->spinBox->value();

    QString lineName=this->ui->lineEdit_addNameLine->text();
    if(stationName.size()==0 || lineName.size()==0)
    {
        return false;
    }
    return true;
}

AddDialog::~AddDialog()
{
    delete ui;
}


