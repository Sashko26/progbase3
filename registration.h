#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include "storage.h"
#include <QString>

namespace Ui {
class registration;
}

class registration : public QDialog
{
    Q_OBJECT

public:
    explicit registration(Storage * _storage,QWidget *parent=0);
    void textBrowserInCorrect();
    QString getLogin();
    QString getPassword();
    QString getRepeadPassword();
    ~registration();

private slots:





private:
    Ui::registration *ui;
    Storage * _storage;

};

#endif // REGISTRATION_H
