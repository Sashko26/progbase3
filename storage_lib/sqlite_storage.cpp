#include "sqlite_storage.h"
#include <QtSql>
#include <QString>
#include <iostream>
#include <metroLine.h>
#include "metroStation.h"
#include "user.h"
#include <QVariant>
#include <QBuffer>
struct haveNotLine
{
    int id;
    bool mitka;
};
QString hashPassword(QString const & pass)
{
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}



bool SqliteStorage::open()
{
    string path=this->dir_name_+"/data.sql";

    QString q_path=QString::fromStdString(path);
 qDebug()<<q_path;
    _db.setDatabaseName(q_path); // set sqlite database file path
    qDebug()<<q_path;
    bool connected = _db.open();  // open db connection
    if (!connected)
    {
        puts(path.c_str());
        puts("ne vidkrylo!");
        return false;
    }
      return true;
}
bool SqliteStorage::close()
{
    _db.close();
    return true;
}
// metroStations

vector<metroStation> SqliteStorage::getAllmetroStations(void)
{
    vector<metroStation> metroStations;
    QSqlQuery query("SELECT * FROM metro_stations WHERE username");
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_stations error:" << query.lastError();
       abort();
    }

    while (query.next())
    {
       metroStation station;
       QString station_name = query.value("station_name").toString();
       int id = query.value("id").toInt();
       int opening_time = query.value("opening_time").toInt();
       QString line_name = query.value("line_name").toString();
      // qDebug() << id << " | " << station_name; qdebug виводить
      //повідомлення в консоль, тому не дуже гарно, коли
      //програма консольна і виводяться технічні повідомлення
       station.id=id;
       station.nameOfStation=station_name.toStdString();
       station.nameOfLine=line_name.toStdString();
       station.hourOfOpening=opening_time;
       metroStations.push_back(station);
    }
    return metroStations;
}
optional<metroStation> SqliteStorage::getmetroStationById(int metroStation_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM metro_stations WHERE id = :id");
    query.bindValue(":id", metroStation_id);
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_station error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
                qDebug() << " found ";
              metroStation station;
              QString station_name = query.value("station_name").toString();
              int id = query.value("id").toInt();
              int opening_time = query.value("opening_time").toInt();
              QString line_name = query.value("line_name").toString();
              qDebug() << id << " | " << station_name;
              station.id=id;
              station.nameOfStation=station_name.toStdString();
              station.nameOfLine=line_name.toStdString();
              station.hourOfOpening=opening_time;
            return station;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updatemetroStation(const metroStation &metroStation)
{

    QSqlQuery query;
    query.prepare("UPDATE metro_stations SET station_name = :station_name , opening_time = :opening_time, line_name = :line_name WHERE id = :id");
    query.bindValue(":station_name", QString::fromStdString(metroStation.nameOfStation));
    cout<<metroStation.hourOfOpening<<endl;
    query.bindValue(":opening_time", metroStation.hourOfOpening);
    query.bindValue(":line_name", QString::fromStdString(metroStation.nameOfLine));
    query.bindValue(":id", metroStation.id);


     if (!query.exec())
    {
        qDebug() << "update station error:" << query.lastError();
        cout<<metroStation.hourOfOpening<<endl;
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;

    }




    return true;
}
bool SqliteStorage::removemetroStation(int metroStation_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM metro_stations WHERE id = :id");
    query.bindValue(":id", metroStation_id);
    if (!query.exec())
    {
        qDebug() << "delete metro_station error:" << query.lastError();
          return false;
    }
    if(query.numRowsAffected()==0)
    {
        qDebug()<< query.numRowsAffected();
        return false;
    }
    QSqlQuery query1;
    query1.prepare("DELETE FROM images WHERE station_id = :station_id");
    query1.bindValue(":station_id", metroStation_id);
    if (!query1.exec())
    {
        qDebug() << "delete image of metro_station error:" << query.lastError();
          return false;
    }
    if(query1.numRowsAffected()==0)
    {
        qDebug()<< query.numRowsAffected();
        return false;
    }
    return true;
}
int SqliteStorage::insertmetroStation(const metroStation &metroStation,int user_id)
{

    QSqlQuery query;


    query.prepare("INSERT INTO metro_stations (station_name, opening_time, line_name, user_id) VALUES (:station_name, :opening_time, :line_name, :user_id)");
    query.bindValue(":station_name", QString::fromStdString(metroStation.nameOfStation));
    query.bindValue(":line_name", QString::fromStdString(metroStation.nameOfLine));
    query.bindValue(":opening_time", metroStation.hourOfOpening);

    query.bindValue(":user_id",user_id);
    if (!query.exec())
    {
        qDebug() << "add metro_station error:" << query.lastError();
    }
    QVariant var= query.lastInsertId();

    return var.toInt();
}




// metroLine
vector<metroLine> SqliteStorage::getAllmetroLines(void)
{
    vector<metroLine> metroLines;
    QSqlQuery query("SELECT * FROM metro_lines");
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_lines error:" << query.lastError();
       abort();
    }
    while (query.next())
    {
       metroLine line;
       QString line_name = query.value("line_name").toString();
       int id = query.value("id").toInt();
       int amount_stations = query.value("amount_stations").toInt();
       QString color_line= query.value("color_line").toString();
       line.id=id;
       line.nameOfline=line_name.toStdString();
       line.colourOfLine=color_line.toStdString();
       line.amountOfstation=amount_stations;
       metroLines.push_back(line);
    }
    return metroLines;

}
optional<metroLine> SqliteStorage::getmetroLineById(int metroLine_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM metro_lines WHERE id = :id");
    query.bindValue(":id", metroLine_id);
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_lines error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
                qDebug() << " found ";
              metroLine line;
              QString line_name = query.value("line_name").toString();
              int id = query.value("id").toInt();
              int amount_stations = query.value("amount_stations").toInt();
              QString color_line = query.value("color_line").toString();
              //qDebug() << id << " | " << line_name;
              line.id=id;
              line.nameOfline=line_name.toStdString();
              line.colourOfLine=color_line.toStdString();
              line.amountOfstation=amount_stations;
            return line;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updatemetroLine(const metroLine &metroLine)
{
    QSqlQuery query;
    query.prepare("UPDATE metro_lines SET line_name = :line_name, amount_stations = :amount_stations, color_line = :color_line WHERE id = :id");
    query.bindValue(":line_name", QString::fromStdString(metroLine.nameOfline));
    query.bindValue(":amount_stations", metroLine.amountOfstation);
    query.bindValue(":color_line", QString::fromStdString(metroLine.colourOfLine));
    query.bindValue(":id", metroLine.id);
 if (!query.exec())
    {
        qDebug() << "update Line error:" << query.lastError();
        return false;

    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }


    return true;
}
bool SqliteStorage::removemetroLine(int metroLine_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM metro_lines WHERE id = :id");
    query.bindValue(":id", metroLine_id);
    if (!query.exec())
    {
        qDebug() << "delete metro_line error:" << query.lastError();
          return false;
    }
    if(query.numRowsAffected()==0)
    {
        qDebug()<< query.numRowsAffected();
        return false;
    }
    return true;
}
int SqliteStorage::insertmetroLine(const metroLine &metroLine)
{
    QSqlQuery query;
    query.prepare("INSERT INTO metro_lines (line_name, amount_stations, color_line) VALUES (:line_name, :amount_stations, :color_line)");
    query.bindValue(":line_name", QString::fromStdString(metroLine.nameOfline));
    query.bindValue(":color_line", QString::fromStdString(metroLine.colourOfLine));
    query.bindValue(":amount_stations", metroLine.amountOfstation);
    if (!query.exec())
    {
        qDebug() << "add metro_station error:" << query.lastError();
    }
    QVariant var= query.lastInsertId();

    return var.toInt();
    return 0;
}

 vector<metroStation> SqliteStorage::getAllUserMetroStations(int user_id, int currentPage, int pageSize, bool searchMode, QString subStr)
 {
      QSqlQuery query;
        vector<metroStation> metroStations;
      if(currentPage==0 && pageSize==0)
      {
            query.prepare("SELECT * FROM metro_stations WHERE user_id = :user_id");
            query.bindValue(":user_id",user_id);
      }
      else
      {
          if(searchMode)
          {
               query.prepare("SELECT * FROM metro_stations "
                            "WHERE user_id = :user_id  AND station_name LIKE ('%' || :some_text  ||'%') LIMIT :pageSize offset :previousStations");
               query.bindValue(":some_text",subStr);
          }
          else
          {
              query.prepare("SELECT * FROM metro_stations "
                             "WHERE user_id = :user_id LIMIT :pageSize offset :previousStations");
          }
          query.bindValue(":user_id", user_id);
          query.bindValue(":pageSize",pageSize);
          query.bindValue(":previousStations",pageSize*(currentPage-1));
       }


          if (!query.exec())
          {
              QSqlError error = query.lastError();
               throw error;
          }

          while (query.next())
          {
              metroStation station;
              station.id = query.value("id").toInt();
              station.nameOfStation = query.value("station_name").toString().toStdString();
              station.nameOfLine = query.value("line_name").toString().toStdString();
              station.hourOfOpening=query.value("opening_time").toInt();
              metroStations.push_back(station);
          }
     return metroStations;
}

// users
 optional<User> SqliteStorage::getUserAuth(QString & username, QString & password)
 {
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash");
    query.bindValue(":username", username);
    query.bindValue(":password_hash", hashPassword(password));
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        qDebug() << error;
        throw error;
    }
    if (query.next())
    {
        qDebug() << "got";
        User user;
        user.id = query.value("id").toInt();
        user.username = query.value("username").toString();
        user.passoword_hash = query.value("password_hash").toString();
        optional<User> opt_value;
        opt_value=user;
        return opt_value;
    }
    return nullopt;
 }

// links



 //need cheking
 vector<metroLine> SqliteStorage::getAllLinesOfstation(int station_id)
 {

     QSqlQuery query;
     query.prepare("SELECT * FROM links WHERE main_entity_id = :main_entity_id");
     query.bindValue(":main_entity_id", station_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     vector<int> id_of_entity;
     while (query.next())
     {
         int num;
         num=query.value("extra_entity_id").toInt();
          id_of_entity.push_back(num);
     }
     vector<metroLine> metroLines;
     for(int i=0;i<static_cast<int>(id_of_entity.size());i++)
     {
        int num =id_of_entity.at(i);
        query.prepare("SELECT * FROM metro_lines WHERE id = :id");
        query.bindValue(":id", num);
        if (!query.exec())
        {
            QSqlError error = query.lastError();
            throw error;
        }

        while (query.next())
        {
                metroLine line;
               line.id=query.value("id").toInt();
               line.nameOfline=query.value("line_name").toString().toStdString();
               line.colourOfLine=query.value("color_line").toString().toStdString();
               line.amountOfstation=query.value("amount_stations").toInt();
               metroLines.push_back(line);
         }

     }

                return metroLines;
 }


 bool SqliteStorage::insertLineinStation(int station_id, int line_id)
 {
     qDebug()<<station_id<<"sdfsdfsdf";
     qDebug()<<line_id<<"sdfsdfsdf";
     QSqlQuery query;

     query.prepare("INSERT INTO links (main_entity_id, extra_entity_id) VALUES (:main_entity_id, :extra_entity_id)");
     qDebug()<<station_id;
     query.bindValue(":main_entity_id",station_id);
     query.bindValue(":extra_entity_id",line_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         qDebug()<< station_id;
         throw error;
     }
     return query.lastInsertId().toBool();





 }
 bool SqliteStorage::removeLineinStation(int station_id, int line_id)
 {
     QSqlQuery query;
     qDebug()<<station_id;
     qDebug()<<line_id;
     query.prepare("DELETE FROM links WHERE main_entity_id = :main_entity_id AND extra_entity_id = :extra_entity_id;");
     query.bindValue(":main_entity_id", station_id);
     query.bindValue(":extra_entity_id", line_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     if(query.numRowsAffected()==0)
     {
         qDebug()<< query.numRowsAffected();
         return false;
     }
        return true;
 }
 vector<metroLine> SqliteStorage::getAllHaveNotLinesOfstation(int station_id)
 {
     QSqlQuery query;
     query.prepare("SELECT * FROM links WHERE main_entity_id = :main_entity_id;");
     query.bindValue(":main_entity_id", station_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     vector<int> id_of_entityHave;
     while (query.next())
     {
         int num;
         num=query.value("extra_entity_id").toInt();
          id_of_entityHave.push_back(num);
     }

     qDebug()<<"id_of_entityHave";
     for(int i=0;i<id_of_entityHave.size();i++)
     {
         qDebug()<<id_of_entityHave[i]<<" ";
     }
     puts("");


      query.prepare("SELECT * FROM links WHERE main_entity_id != :main_entity_id;");
     query.bindValue(":main_entity_id", station_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     vector<int> id_of_entity;
     while (query.next())
     {
         int num;
         num=query.value("extra_entity_id").toInt();
          id_of_entity.push_back(num);
     }

          sort(id_of_entity.begin(), id_of_entity.end()); // сортирум вектор
          vector<int> :: iterator it;
          it = unique(id_of_entity.begin(), id_of_entity.end()); // удаляем повторяющиеся элементы
          id_of_entity.resize(it - id_of_entity.begin());

         vector<haveNotLine> haveNotId;
         for(int i=0;i<id_of_entity.size();i++)
         {
             haveNotLine haveNot;
             haveNot.id=id_of_entity[i];
             haveNot.mitka=true;
             haveNotId.push_back(haveNot);
         }
         for(int i=0;i<haveNotId.size();i++)
         {
             for(int j=0;j<id_of_entityHave.size();j++)
             {
                 if(haveNotId[i].id==id_of_entityHave[j])
                 {
                     haveNotId[i].mitka=false;
                 }
             }
         }
         id_of_entity.clear();

         for(int i=0;i<haveNotId.size();i++)
         {
             if(haveNotId[i].mitka==true)
             {
                 id_of_entity.push_back(haveNotId[i].id);
             }
         }

            qDebug()<<"hell";
         for(int i=0;i<id_of_entity.size();i++)
         {
            qDebug()<<id_of_entity[i];

         }
      vector<metroLine> metroLines;
     for(int i=0;i<static_cast<int>(id_of_entity.size());i++)
     {
         qDebug()<< id_of_entity.size();
        int num =id_of_entity.at(i);
        query.prepare("SELECT * FROM metro_lines WHERE id = :id;");
        query.bindValue(":id", num);
        if (!query.exec())
        {
            QSqlError error = query.lastError();
            throw error;
        }

        while (query.next())
        {
                metroLine line;
               line.id=query.value("id").toInt();
               line.nameOfline=query.value("line_name").toString().toStdString();
               line.colourOfLine=query.value("color_line").toString().toStdString();
               line.amountOfstation=query.value("amount_stations").toInt();
               metroLines.push_back(line);
         }

     }
        return metroLines;
 }

 void SqliteStorage::amountLinks(int station_id)
 {
     QSqlQuery query;
     query.prepare("SELECT * FROM links WHERE main_entity_id = :main_entity_id;");
     query.bindValue(":main_entity_id", station_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     int value=0;
     while (query.next())
     {
         value++;
     }
     this->amountlinks=value;
 }
 optional<User> SqliteStorage::creatingNewAccount(QString & password, QString & passwordRepead, QString & login)
  {
              if(password!=passwordRepead)
              {
                  qDebug("password != passwordRepead");
                 return nullopt;
              }
           QSqlQuery query;
           int iter =0;
           query.prepare("SELECT * FROM users WHERE username = :username");
           query.bindValue(":username",login);
           if (!query.exec())
           {
               QSqlError error = query.lastError();
               throw error;
           }
            qDebug()<<login;
        while(query.next())
           {
              iter++;
           }
           if(iter!=0)
           {
               return nullopt;
           }
               qDebug()<<iter;
           query.prepare("SELECT * FROM users WHERE password_hash = :password_hash");
           query.bindValue(":password_hash",hashPassword(password));
           if (!query.exec())
           {
               QSqlError error = query.lastError();
               throw error;
           }
           qDebug()<<QString::number(query.size());

           while(query.next())
           {
              iter++;
           }
           if(iter!=0)
           {
               return nullopt;
           }
           query.prepare("INSERT INTO users (username, password_hash) VALUES (:username, :password_hash)");
           query.bindValue(":username",login);

           query.bindValue(":password_hash",hashPassword(password));
           if (!query.exec())
           {
               QSqlError error = query.lastError();
               throw error;
           }
              query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash");
              query.bindValue(":username",login);
              query.bindValue(":password_hash",hashPassword(password));
              if (!query.exec())
              {
                  QSqlError error = query.lastError();
                  throw error;
              }
              User user;
              while(query.next())
              {
                  user.id=query.value("id").toInt();
                  user.username=query.value("username").toString();
                  user.passoword_hash=query.value("password_hash").toString();
                  qDebug()<<user.id;
                  qDebug()<<user.username;
                  qDebug()<<user.passoword_hash;
              }
              optional<User> opt_value =user;



     return opt_value;
  }
    int  SqliteStorage::getAmountUserStation(int user_id,bool searchMode,QString subStr)
  {
      QSqlQuery query;
      int amountUserStation;
      if(searchMode)
      {
          query.prepare("SELECT COUNT(*) FROM metro_stations WHERE user_id= :user_id AND station_name LIKE ('%' || :some_text  ||'%')");
          query.bindValue(":some_text",subStr);
      }
      else
      {
          query.prepare("SELECT COUNT(*) FROM metro_stations WHERE user_id = :user_id;");
      }
      query.bindValue(":user_id",user_id);
      if(!query.exec())
      {
          QSqlError error = query.lastError();
          throw error;
      }
        while(query.next())
      {
         amountUserStation=query.value("COUNT(*)").toInt();
         qDebug()<<"tse pyprts!";
         qDebug()<<amountUserStation;
         qDebug()<<subStr;
      }
      return amountUserStation;
  }

  vector<metroStation> SqliteStorage::searchForSubStr(const QString &arg1,int pageSize,int currentPageSearch_mode)
  {
      qDebug()<<currentPageSearch_mode;
      qDebug()<<arg1;
      QSqlQuery query;
      query.prepare("SELECT * FROM metro_stations WHERE user_id= :user_id AND station_name LIKE ('%' || :some_text  ||'%') LIMIT :pageSize offset :previousStations");
      query.bindValue(":user_id", user_id);
      query.bindValue(":pageSize",pageSize);
     query.bindValue(":previousStations",pageSize*(currentPageSearch_mode-1));
      query.bindValue(":some_text",arg1);
      if (!query.exec())
      {
          QSqlError error = query.lastError();
           throw error;
      }
      vector<metroStation> stations;
      while(query.next())
      {
          metroStation station;
          station.id=query.value("id").toInt();
          station.hourOfOpening=query.value("opening_time").toInt();
          station.nameOfStation=query.value("station_name").toString().toStdString();
          station.nameOfLine=query.value("line_name").toString().toStdString();
          stations.push_back(station);
      }
      return stations;
  }
bool SqliteStorage::insertPhoto(int id, QString path)
{

    QPixmap image(path);
    QByteArray bArray;
    QBuffer buffer(&bArray);
    buffer.open(QIODevice::WriteOnly);
    image.save(&buffer,"JPG");
    QSqlQuery query;
    query.prepare("INSERT INTO images (image,station_id) Values (:image,:station_id)");
    query.bindValue(":station_id",id);
    query.bindValue(":image",bArray);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        qDebug()<<query.lastError();
         throw error;
    }






//    QSqlQuery query;
//    query.prepare("UPDATE metro_stations SET imagine = :imagine WHERE id= :id");
//    query.bindValue(":imagine",arr);
//    query.bindValue(":id",id);


//    if (!query.exec())
//    {
//        QSqlError error = query.lastError();
//        qDebug()<<query.lastError();
//        throw error;
//    }
    return true;

}
QByteArray SqliteStorage::getPhoto(int id)
{
    QSqlQuery query;
    qDebug()<<id;
    query.prepare("SELECT image FROM images WHERE station_id=:station_id");
    query.bindValue(":station_id",id);

    if (!query.exec())
    {
        QSqlError error = query.lastError();
         throw error;
    }
    QByteArray arr;

    while(query.next())
    {
       // if(arr.isNull())
       arr=query.value("image").toByteArray();
       if(arr.isNull())
           qDebug()<<"good buy mazafucker";
    }
    return arr;
}






