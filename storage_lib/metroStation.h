#pragma once
#include <vector>
#include <string>
#include <QMetaType>
#include <QPixmap>
using std::vector;
using std::string;
typedef struct metroStation
{
    int id;
    string nameOfStation;
    int hourOfOpening;
    string nameOfLine;
    int user_id;


} metroStation;
Q_DECLARE_METATYPE(metroStation)


