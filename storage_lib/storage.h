#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "metroStation.h"
#include "metroLine.h"
#include "user.h"

using namespace std;

class Storage
{
 public:
    virtual ~Storage() {}
   virtual bool open() = 0;
   virtual bool close() = 0;
   // metroStations
   virtual vector<metroStation> getAllmetroStations(void) = 0;
   virtual optional<metroStation> getmetroStationById(int metroStation_id) = 0;
   virtual bool updatemetroStation(const metroStation &metroStation) = 0;
   virtual bool removemetroStation(int metroStation_id) = 0;
   virtual bool insertPhoto(int id, QString path) =0;
   virtual QByteArray getPhoto(int id) =0;

    //довбня з user_id;
   virtual int insertmetroStation(const metroStation &metroStation, int user_id) = 0;
   // metroLine
   virtual vector<metroLine> getAllmetroLines(void) = 0;
   virtual optional<metroLine> getmetroLineById(int metroLine_id) = 0;
   virtual bool updatemetroLine(const metroLine &metroLine) = 0;
   virtual bool removemetroLine(int metroLine_id) = 0;
   virtual int insertmetroLine(const metroLine &metroLine) = 0;

       virtual vector<metroStation> getAllUserMetroStations(int user_id, int currentPage=0, int pageSize=0, bool searchMode=false,QString subStr="x")= 0;
       virtual int getAmountUserStation(int user_id=0, bool searchMode=false, QString subStr="x") = 0;
       virtual vector<metroStation> searchForSubStr(const QString &arg1,int pageSize,int currentPage) = 0;


    // users
     virtual optional<User> getUserAuth(QString & username, QString & password) = 0;
     // links
     virtual vector<metroLine> getAllLinesOfstation(int station_id) = 0;
     virtual bool insertLineinStation(int station_id, int line_id) = 0;
     virtual bool removeLineinStation(int station_id, int line_id)= 0;
    virtual vector<metroLine> getAllHaveNotLinesOfstation(int station_id) = 0;
    virtual void amountLinks(int station_id) = 0;
       virtual optional<User> creatingNewAccount(QString & password, QString & passwordRepead, QString & login) = 0;


    int user_id;
    int metroStation_id;
    int amountlinks;

};
