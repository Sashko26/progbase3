#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include "storage.h"
#include <QSqlDatabase>
#include "metroLine.h"
class SqliteStorage : public Storage
{
    QSqlDatabase _db;
    const string dir_name_;

public:
    SqliteStorage(const string &dir_name) : dir_name_(dir_name)
    {
        _db = QSqlDatabase::addDatabase("QSQLITE");
    }



   bool open() ;
   bool close() ;
  // metroStations
   vector<metroStation> getAllmetroStations(void) ;
   optional<metroStation> getmetroStationById(int metroStation_id) ;
   bool updatemetroStation(const metroStation &metroStation) ;
   bool removemetroStation(int metroStation_id) ;
   int insertmetroStation(const metroStation &metroStation, int user_id);
  // metroLine
   vector<metroLine> getAllmetroLines(void) ;
   optional<metroLine> getmetroLineById(int metroLine_id) ;
   bool updatemetroLine(const metroLine &metroLine) ;
   bool removemetroLine(int metroLine_id) ;
   int insertmetroLine(const metroLine &metroLine) ;
   vector<metroLine> getAllHaveNotLinesOfstation(int station_id);
   void amountLinks(int station_id);
   bool insertPhoto(int id, QString path);
   QByteArray getPhoto(int id);
   //int user_id;




     vector<metroStation>  getAllUserMetroStations(int user_id, int currentPage=0, int pageSize=0, bool searchMode=false,QString subStr="x");
      int getAmountUserStation(int user_id=0, bool searchMode=false, QString subStr="x");
      vector<metroStation> searchForSubStr(const QString &arg1, int pageSize, int currentPage);

// users
  optional<User> getUserAuth(QString & username, QString & password);
 // links
  vector<metroLine> getAllLinesOfstation(int station_id) ;
  bool insertLineinStation(int station_id, int line_id) ;
  bool removeLineinStation(int station_id, int line_id);
  optional<User> creatingNewAccount(QString & password, QString & passwordRepead, QString & login);
};


#endif // SQLITE_STORAGE_H
